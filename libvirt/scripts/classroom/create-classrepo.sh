#!/bin/bash
yum install -y createrepo yum-utils

mkdir /var/www/html/repo
cat > /root/packages.txt << EOF
vim
elinks
yum-utils
bash-completion
postfix
mariadb-server
targetcli
iscsi-initiator-utils
nfs-utils
samba
samba-client
httpd
httpd-manual
mod_wsgi
mod_ssl
cifs-utils
EOF

while read x; do repotrack -a x86_64 -p /var/www/html/repo/ $x ;done  < /root/packages.txt
createrepo -v --repo classroom-repo /var/www/html/repo 
##Create Groups##
yum-groups-manager -n "Netork File System Client" --save=/var/www/html/repo/mygroups.xml --mandatory autofs cgdcbxd iscsi-initiator-utils nfs4-acl-tools samba-client targetcli
yum-groups-manager -n "File and Storage Server" --save=/var/www/html/repo/mygroups.xml --mandatory nfs4-acl-tools samba targetcli targetd

createrepo --update -g /var/www/html/repo/mygroups.xml /var/www/html/repo/
###

systemctl reload httpd


