This is a project to create a RHCE lab environment to study from. This was built from freezo/lab and there are a total of 3 machines. Go into the directory for your provider and use the vagrant up command to initialize the lab. 

3 VMs:
- classroom 
- server
- desktop

It is suggested that you avoid rebuilding the classroom server. 

The practice exam is made available from the classroom server. On a browser from you host machine it should be accessible at http://localhost:8880/exam.html


USE:

CD into our repective provider (libvirt/virtualbox) and vagrant init. 
Then # vagrant up (use the --no-parallel option if you want to have the VMs built in sequence) 
